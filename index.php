<?php

use application\core\Router;


try {
	include 'autoload.php';
	$router = new Router();
	$router->run();
} catch (Exception $e) {
	echo json_encode([$e->getCode().'  '.$e->getMessage()]);
}



