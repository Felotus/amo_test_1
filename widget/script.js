define(['jquery'], function ($) {
  var CustomWidget = function () {
    var self = this;
    this.callbacks = {
      render: function () {
        console.log('render');


        var html_data = '<a id="my_getcsv">выгрузить сделки в CSV</a>';
        self.render_template(
          {
            caption: {
              class_name: 'new_widget', //имя класса для обертки разметки
            },
            body: html_data,//разметка
            render: '' //шаблон не передается
          }
        );


        return true;
      },
      init: function () {
        console.log('init');
        return true;
      },
      bind_actions: function () {
        console.log('bind_actions');
        return true;
      },
      settings: function () {
        return true;
      },
      onSave: function () {
        return true;
      },
      destroy: function () {

      },
      contacts: {
        //select contacts in list and clicked on widget name
        selected: function () {
          console.log('contacts');
        }
      },
      leads: {
        //select leads in list and clicked on widget name
        selected: function () {
          console.log('leads');

          var l_data = self.list_selected().selected;
          var ids = [];

          for (var i = 0; i < l_data.length; i++) {
            ids[i] = l_data[i].id;
          }
          var id = ids.join();
          $.ajax({
            type: 'POST',
            url: "http://localhost/grade_2",
            data: 'ids=' + id,
            success: function (result) {
              var type = 'data:application/octet-stream;base64, ';
              result = result.split("&").join("\r\n");
              console.log(result);
              var base = btoa(unescape(encodeURIComponent(result)));
              console.log(base);
              var res = type + base;
              $("#my_getcsv").attr('download', 'file.csv');
              $("#my_getcsv").attr('href', res);
            }
          });
        }
      },
      tasks: {
        //select taks in list and clicked on widget name
        selected: function () {
          console.log('tasks');
        }
      }
    };
    return this;
  };

  return CustomWidget;
});
