<?php
return [
	'task_1' => [
		'controller' => 'main',
		'action' => 'mass_create',
	],
	'task_2' => [
			'controller' => 'main',
			'action' => 'create_textfield',
	],
	'task_3' => [
		'controller' => 'main',
		'action' => 'add_event',
	],
	'task_4' => [
		'controller' => 'main',
		'action' => 'add_task',
	],
	'task_5' => [
		'controller' => 'main',
		'action' => 'close_task',
	],
	'task_6' => [
		'controller' => 'main',
		'action' => 'delite_multi',
	],
	'grade_2' => [
		'controller' => 'main',
		'action' => 'widget',
	],
	'' => [
		'controller' => 'main',
		'action' => 'index',
	],
];

