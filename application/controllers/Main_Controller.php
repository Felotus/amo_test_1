<?php

namespace application\controllers;

use application\core\Controller;
use application\core\View;
use application\lib\Data_Helper;
use application\lib\Eternity_Factory;
use application\lib\Amo_CRUD;
use application\lib\Curl_Req;
use application\models\Field;
use application\models\Contact;
use application\models\Company;
use application\models\Customer;
use application\models\Note;
use application\models\Task;
use application\models\Lead;
use Exception;


class Main_Controller extends Controller {

	const CALL_DURATION = 30;
	const CALL_LINK = 'http://example.com/calls/1.mp3';
	const TEXTF_NAME = 'текстовое поле';
	const ROW_START = 0;
	const MULTI_NAME = 'мультиполе';
	const SCRIPTS = ['jq_min', 'test'];
	const TITLE_SEL_TASK_TYPE = 'тип запроса';
	const HEADER_SEL_TASK_TYPE = 'тип запроса';
	const STYLES = ['main'];
	const PAGE_VIEW = 'Main.index';
	private $_elem_factory;
	private $_CRUD;


		public function __construct(){
			parent::__construct();
			$this->_CRUD = new Amo_CRUD();
			$this->_elem_factory = new Eternity_Factory($this->_CRUD);
		}

	public function index_action() {
		if ($_SERVER['REQUEST_METHOD'] === 'GET') {
			$task_types = $this->_CRUD->get_task_types();
			$sel_task_types = [
				'title' => self::TITLE_SEL_TASK_TYPE,
				'header' => self::HEADER_SEL_TASK_TYPE,
				'options' => $task_types,
			];

			$this->_view->set_scripts(self::SCRIPTS)
				->set_styles(self::STYLES)
				->render(self::PAGE_VIEW,'главная страница', $sel_task_types);
		} else {
			View::errorCode(404);
		}
	}

	public function mass_create_action() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$enums_val = [
				'значение 1',
				'значение 2',
				'значение 3',
				'значение 4',
				'значение 5',
				'значение 6',
				'значение 7',
				'значение 8',
				'значение 9',
				'значение 10',
			];
			$field_name = self::MULTI_NAME;
			$num = Data_Helper::clear($_POST['num']);
			$multi = $this->_elem_factory->create_elem('field');
			$multi->set_name($field_name)
				->set_enums($enums_val)
				->set_type(Field::MULTISELECT)
				->set_elem_type(Contact::ELEM_TYPE)
				->set_editable(Field::EDITABLE)
				->create();
			$multi->find_enums();
			$contacts = $this->_CRUD->list_contacts();
			foreach ($contacts as $value) {
				$enums_values = [];
				$multiCont = clone($multi);
				foreach ($multi->get_enums() as $key => $val) {
					if (mt_rand(0, 1) === 1) {
						$enums_values[] = $key;
					}
				}
				$multiCont->set_values($enums_values);
				$value->set_custom_fields([$multiCont]);
			}
			$this->_CRUD->update_contacts($contacts);
			$contacts = $this->_elem_factory->create_mass_elem('Contact', $num);
			foreach ($contacts as $k => $v){
				$enums_data = [];
				$multiCont = clone($multi);
				foreach ($multi->get_enums() as $key => $val) {
					if (mt_rand(0, 1) === 1) {
						$enums_data[] = $key;
					}
				}
				$multiCont->set_values($enums_data);
				$v->set_name('contact'.$k)
					->set_custom_fields([$multiCont]);

			}

			$this->_CRUD->create_contacts($contacts);
			foreach ($contacts as $k => $v) {
					$companies[$k] = $this->_elem_factory->create_elem('Company');
					$companies[$k]->set_name('Company'.$k)
						->set_contacts([$contacts[$k]->get_id()]);
			}
			$this->_CRUD->create_companies($companies);
			foreach ($contacts as $k => $v) {
				$leads[$k] = new lead($this->_CRUD);
				$leads[$k]->set_name('Lead'.$k)
					->set_contacts([$contacts[$k]->get_id()])
					->set_company($companies[$k]->get_id());
			}
			$this->_CRUD->create_leads($leads);
			foreach ($contacts as $k => $v) {
				$customers[$k] = new Customer($this->_CRUD);
				$customers[$k]->set_name('Customer'.$k)
					->set_contacts([$contacts[$k]->get_id()])
					->set_company($companies[$k]->get_id());
			}
			$this->_CRUD->create_customers($customers);
			echo 'готово';
		} else {
			View::errorCode(404);
		}
	}

	public function create_textfield_action(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$field_name = self::TEXTF_NAME;
			$elem_id = Data_Helper::clear($_POST['id']);
			$elem = $this->_elem_factory->create_elem_by_type(Data_Helper::clear($_POST['elem_type']));
			$elem->set_id($elem_id);
			$field =  $this->_elem_factory->create_elem('Field');
			$field->set_id(NULL)
				->set_type(Field::TEXT)
				->set_name($field_name)
				->set_values([Data_Helper::clear($_POST['text'])])
				->set_elem_type($elem->get_type());

			foreach ($field->get_list() as $v) {
				if ($v->get_type() === $field->get_type()) {
					$field->set_id($v->get_id());
				}
			};
			if (is_null($field->get_id())) {
				$field->create();
			}
			$elem->set_custom_fields([$field])
				->update();
			echo 'готово';
		} else {
			View::errorCode(404);
		}
	}

	public function add_event_action(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$elem_id = Data_Helper::clear($_POST['id']);
			$elem = $this->_elem_factory->create_elem_by_type(Data_Helper::clear($_POST['elem_type']));
			$elem->set_id($elem_id);
			$note = $this->_elem_factory->create_elem('Note');
			$note->set_type(Data_Helper::clear($_POST['note_type']))
				->set_val(Data_Helper::clear($_POST['text']))
				->set_call_link(self::CALL_LINK)
				->set_call_duration(self::CALL_DURATION)
				->set_elem_id($elem->get_id())
				->set_elem_type($elem->get_type())
				->create();
			echo 'готово';
		} else {
			View::errorCode(404);
		}
	}

	public function add_task_action(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$elem_id = Data_Helper::clear($_POST['id']);
			$elem = $this->_elem_factory->create_elem_by_type(Data_Helper::clear($_POST['elem_type']));
			$elem->set_id($elem_id);
			$task = $this->_elem_factory->create_elem('Task');
			$task->set_type(Data_Helper::clear($_POST['task_type']))
				->set_val(Data_Helper::clear($_POST['text']))
				->set_date(Data_Helper::clear($_POST['date']))
				->set_elem_id($elem->get_id())
				->set_elem_type($elem->get_type())
				->create();
			echo 'готово';
		} else {
			View::errorCode(404);
		}
	}

	public function close_task_action(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$task = $this->_elem_factory->create_elem('Task');
			$task->set_val(Data_Helper::clear($_POST['text']))
				->set_id(Data_Helper::clear($_POST['id']))
				->close();
			echo 'готово';
		} else {
			View::errorCode(404);
		}
	}

	private function model_to_lib_array_for_csv(array $elems){
		$result = [];
		foreach ($elems as $elem) {
			$result[$elem->get_id()] = $elem->get_name();
		}
		return $result;
	}

	public function widget_action(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			if (isset($_POST["ids"]) && (count($_POST["ids"]) > 0)) {



				header('Access-Control-Allow-Origin: *');
				$ids = explode(',', $_POST['ids']);
				$companies = [];
				$contacts = [];
				$leads = $this->_CRUD->list_leads_ids($ids);
				foreach ($leads as $lead) {
					if (!in_array($lead->get_company(), $companies)) {
						$companies[] = $lead->get_company();
					}
					foreach ($lead->get_contacts() as $contact_id) {
						if (!in_array($contact_id, $contacts)) {
							$contacts[] = $contact_id;
						}
					}
				}
				$companies = $this->model_to_lib_array_for_csv($this->_CRUD->list_companies_ids($companies));
				$contacts = $this->model_to_lib_array_for_csv($this->_CRUD->list_contacts_ids($contacts));

				$line_out = '';
				foreach ($leads as $lead) {
					$tags = implode('. ', $lead->get_tags());
					$contacts_id = $lead->get_contacts();
					$custom_fields = '';
					foreach ($lead->get_custom_fields() as $custom_field) {
						$custom_fields .= ' поле - ';
						$custom_fields .= $custom_field->get_name() . ' ';
						$custom_fields .= '. знчаения - ';
						foreach ($custom_field->get_values() as $values) {
							$custom_fields .= $values . '. ';
						}

						$custom_fields = substr($custom_fields, 0, -2);
						$custom_fields .= ' ----- ';
					}
					$line_out .= 'name: ' . $lead->get_name() . '; date: ' . date('d.m.Y H:i:s', $lead->get_create_date()) . '; tags : ' . $tags . '; fields: ' . $custom_fields . '; company: ' . $companies[$lead->get_company()] . '; contacts: ';
					foreach ($contacts_id as $contact_id) {
						$line_out .= $contacts[$contact_id] . '. ';
					}
					$line_out = substr($line_out, 0, -2);
					$line_out .= '; ';
					$line_out .= '&';
				}

				echo $line_out;
			}
		} else {
			View::errorCode(404);
		}

	}



}
