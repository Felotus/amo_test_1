<?php

namespace application\core;

use application\lib\EternityFactory;
use application\lib\Amo_CRUD;
use Exception;

abstract class Controller {

	protected $_view;

	/**
	 * Controller constructor.
	 *
	 * @param array $route
	 *
	 * @throws Exception
	 */
	public function __construct(){
		$this->_view = new View();
	}
}

