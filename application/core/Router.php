<?php

namespace application\core;

use application\core\View;
use application\lib\Data_Helper;

class Router {

	private $_routes;
	private $_params;
	/**
	 * Router constructor.
	 */
	public function __construct(){
		$routs = Data_Helper::get_config('routs');
		foreach ($routs as $key => $val) {
			$this->add($key, $val);
		}
	}

	/**
	 * @param $route
	 * @param $params
	 */
	private function add($route, $params){
		$route = '#^'.$route.'$#';
		$this->_routes[$route] = $params;
	}

	/**
	 *
	 */
	public function run(){
		if ($this->match()) {
			$path = 'application\controllers\\'.ucfirst($this->_params['controller']).'_Controller';
			if (class_exists($path)) {
				$action = $this->_params['action'].'_action';
				if (method_exists($path, $action)){
					$controller = new $path($this->_params);
					$controller->$action();
				} else {
					View::errorCode(405);
				}
			} else {
				View::errorCode(403);
			}
		} else {
			View::errorCode(404);
		}
	}

	/**
	 * @return bool
	 */
	private function match(){
		$uri = trim($_SERVER['REQUEST_URI'], '/');
		$result = FALSE;
		foreach ($this->_routes as $route => $params){
			if (preg_match($route, $uri, $matches)){
				$this->_params = $params;
				$result = TRUE;
				break;
			}
		}
		return $result;
	}
}


