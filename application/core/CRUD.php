<?php
namespace application\core;

interface CRUD {

	/**
	 * @param array Contact $contacts
	 *
	 * @return array Contact
	 */
	public function create_contacts(array $contacts);

	/**
	 * @param array Company $companies
	 *
	 * @return array Company
	 */
	public function create_companies(array $companies);

	/**
	 * @param array Customer $customers
	 *
	 * @return array Customer
	 */
	public function create_customers(array $customers);

	/**
	 * @param array Lead $leads
	 *
	 * @return array Lead
	 */
	public function create_leads(array $leads);

	/**
	 * @param array Field $fields
	 *
	 * @return array Field
	 */
	public function create_fields(array $fields);

	/**
	 * @param array Task $tasks
	 *
	 * @return array Task
	 */
	public function create_tasks(array $tasks);


	/**
	 * @param array $notes
	 *
	 * @return mixed
	 */
	public function create_notes(array $notes);

	/**
	 * @param array Contact $contacts
	 *
	 * @return array Contact
	 */
	public function update_contacts(array $contacts);

	/**
	 * @param array Company $companies
	 *
	 * @return array Company
	 */
	public function update_companies(array $companies);

	/**
	 * @param array Customer $customers
	 *
	 * @return array Customer
	 */
	public function update_customers(array $customers);

	/**
	 * @param array Lead $leads
	 *
	 * @return array Lead
	 */
	public function update_leads(array $leads);

	/**
	 * @param array Task $tasks
	 *
	 * @return array Task
	 */
	public function update_tasks(array $tasks);

	/**
	 * @return array
	 */
	public function get_task_types();

	/**
	 * @param $first_row
	 *
	 * @return array Contact
	 */
	public function list_contacts($first_row);

	/**
	 * @param $first_row
	 *
	 * @return array Company
	 */
	public function list_companies($first_row);

	/**
	 * @param $first_row
	 *
	 * @return array Customer
	 */
	public function list_customers($first_row);

	/**
	 * @param $first_row
	 *
	 * @return array Lead
	 */
	public function list_leads($first_row);

	/**
	 * @param array Field $fields
	 *
	 * @return mixed
	 */
	public function delete_fields(array $fields);

	/**
	 * @param int $elem_type
	 *
	 * @return mixed
	 */
	public function list_fields($elem_type);
}
