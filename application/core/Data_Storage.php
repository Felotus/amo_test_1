<?php

namespace application\core;

interface Data_Storage {


	/**
	 * @param string $link
	 * @param array $data
	 * @param array $params
	 *
	 * @return array
	 */
	static public function query($link, array $data, array $params);

}
