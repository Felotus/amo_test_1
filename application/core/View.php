<?php

namespace application\core;

class View {

	private $_path;
	private $_layout = 'default';
	private $_styles = [];
	private $_scripts = [];


	/**
	 * @param array $styles
	 *
	 * @return $this
	 */
	public function set_styles(array $styles){
		$this->_styles = $styles;
		return $this;
	}


	/**
	 * @param array $scripts
	 *
	 * @return $this
	 */
	public function set_scripts(array $scripts){
		$this->_scripts = $scripts;
		return $this;
	}

	/**
	 * @param string $val
	 *
	 * @return $this
	 */
	public function set_layout($val){
		$this->_layout = $val;
		return $this;
	}

	/**
	 * @param string $title
	 * @param array $vars
	 */
	public function render($path, $title, $vars = []){
		$path = 'application/views/' . str_replace('.', '/', $path) . '.php';
		$scripts = '';
		$styles = '';
		foreach ($this->_styles as $val){
			$val = 'public/styles/'.$val.'.css';
			if (file_exists($val)){
				$styles .= '<link rel="stylesheet"  href='.$val.' >';
			}
		}
		foreach ($this->_scripts as $val){
			$val = 'public/scripts/'.$val.'.js';
			if (file_exists($val)){
				$scripts .= '<script "type=text/javascript" src='.$val.'></script>';
			}
		}
		if (file_exists($path)){
			ob_start();
			require $path;
			$content = ob_get_clean();
			require 'application/views/layouts/' . $this->_layout . '.php';
		} else {
			echo 'вид не найден '.$this->_path;
		}
	}

	/**
	 * @param int $code
	 */
	static public function errorCode($code){
		$path = 'application/views/errors/'.$code.'.php';
		http_response_code($code);
		if (file_exists($path)){
			require $path;
		} else {
			require 'application/views/errors/unknown.php';
		}

	}

	/**
	 * @param string $url
	 */
	public function  redirect($url){
		header('locarion'.$url);
		exit;
	}
}
