<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title; ?></title>
        <?php echo $styles; ?>
    </head>
    <body>
        <?php echo $content; ?>
        <?php echo $scripts; ?>
    </body>
</html>

