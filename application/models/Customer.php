<?php

namespace application\models;

class Customer extends Main_Elem {
	private $_contacts;
	private $_company;
	const ELEM_TYPE = 12;

	/**
	 * @param array $contacts_id
	 *
	 * @return $this
	 */
	public function set_contacts(array $contacts_id){
		$this->_contacts = $contacts_id;
		return $this;
	}

	/**
	 * @param $company_id
	 *
	 * @return $this
	 */
	public function set_company($company_id){
		$this->_company = $company_id;
		return $this;
	}

	/**
	 * @return array Contact|NULL
	 */
	public function get_contacts(){
		return $this->_contacts;
	}

	/**
	 * @return Company|NULL
	 */
	public function get_company(){
		return $this->_company;
	}

	/**
	 * @return $this
	 */
	public function update(){
		$this->_CRUD->update_customers([$this]);
		return $this;
	}


	/**
	 * @return $this
	 */
	public function create(){
		$this->_CRUD->create_customers([$this]);
		return $this;
	}

	/**
	 * @param $first_row
	 *
	 * @return array Customer
	 */
	public function get_list($first_row = 0){
		return $this->_CRUD->list_Customers($first_row);
	}
}
