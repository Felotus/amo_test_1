<?php

namespace application\models;

use application\core\CRUD;
use application\models\Base_Elem;

class Field extends Base_Elem {
	const EDITABLE = 1;
	const TEXT = 1;
	const MULTISELECT = 5;
	private $_name;
	private $_enums;
	private $_type;
	private $_editable;
	private $_values;

	private $_elem_type;


	/**
	 * @return array|NULL
	 */
	public function get_values(){
		return $this->_values;
	}

	/**
	 * @param array $values
	 *
	 * @return $this
	 */
	public function set_values(array $values){
		$this->_values = $values;
		return $this;
	}

	/**
	 * @return int|NULL
	 */
	public function get_editable(){
		return $this->_editable;
	}

	/**
	 * @param int $editable
	 *
	 * @return $this
	 */
	public function set_editable($editable){
		$this->_editable = $editable;
		return $this;
	}


	/**
	 * @return array|NULL
	 */
	public function get_enums(){
		return $this->_enums;
	}

	/**
	 * @param array $enums
	 *
	 * @return $this
	 */
	public function set_enums(array $enums){
		$this->_enums = $enums;
		return $this;
	}

	/**
	 * @return string|NULL
	 */
	public function get_name(){
		return $this->_name;
	}

	/**
	 * @param string $name|NULL
	 *
	 * @return $this
	 */
	public function set_name($name){
		$this->_name = $name;
		return $this;
	}




	/**
	 * @return int|NULL
	 */
	public function get_type(){
		return $this->_type;
	}

	/**
	 * @param int $type
	 *
	 * @return $this
	 */
	public function set_type($type){
		$this->_type = $type;
		return $this;
	}

	/**
	 * @return int|NULL
	 */
	public function get_elem_type(){
		return $this->_elem_type;
	}

	/**
	 * @param int $type
	 *
	 * @return $this
	 */
	public function set_elem_type($type){
		$this->_elem_type = $type;
		return $this;
	}

	/**
	 * @param int $elem_type
	 *
	 * @return $this
	 */
	public function create(){
		$this->_CRUD->create_fields([$this]);
		return $this;
	}


	/**
	 * @return bool
	 */
	public function delete(){
		$this->_CRUD->delete_fields([$this]);
		return TRUE;
	}



	/**
	 * @return array Field
	 */
	public function get_list(){
		return $this->_CRUD->list_fields($this->_elem_type);
	}


	/**
	 * @return bool
	 */
	public function find_enums(){
		$result = FALSE;
		foreach ($this->get_list($this->_elem_type) as $k => $v) {
			if ($v->get_id() === $this->get_id()) {
				$this->set_enums($v->get_enums());
				$result = TRUE;
				break;
			}
		}
		return $result;
	}
}

