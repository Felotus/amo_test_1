<?php

namespace application\models;

class Lead extends Main_Elem {
	const ELEM_TYPE = 2;
	private $_contacts;
	private $_company;
	private $_tags;


	/**
	 * @return array Contact|NULL
	 */
	public function get_tags(){
		return $this->_tags;
	}

	/**
	 * @param array $tags_ids
	 *
	 * @return $this
	 */
	public function set_tags(array $tags_ids){
		$this->_tags = $tags_ids;
		return $this;
	}

	/**
	 * @return array Contact|NULL
	 */
	public function get_contacts(){
		return $this->_contacts;
	}

	/**
	 * @param array $contacts_id
	 *
	 * @return $this
	 */
	public function set_contacts(array $contacts_id){
		$this->_contacts = $contacts_id;
		return $this;
	}

	/**
	 * @return Company|NULL
	 */
	public function get_company(){
		return $this->_company;
	}

	/**
	 * @param int $company_id
	 *
	 * @return $this
	 */
	public function set_company($company_id){
		$this->_company = $company_id;
		return $this;
	}

	/**
	 * @param array Lead $contacts
	 */
	public function update(){
		$this->_CRUD->update_leads([$this]);
	}

	/**
	 * @param array Lead $contacts
	 */
	public function create(){
		$this->_CRUD->create_leads([$this]);
	}


	/**
	 * @param $first_row
	 *
	 * @return array Lead
	 */
	public function get_list($first_row = 0){
		return $this->_CRUD->list_leads($first_row);
	}
}

