<?php

namespace application\models;

use application\core\CRUD;

class Task extends Base_Elem {
	private $_type;
	private $_val;
	private $_date;
	private $_complited;
	private $_elem_id;
	private $_elem_type;
	const COMPLITED = 1;


	/**
	 * @param int $elem_type
	 *
	 * @return $this
	 */
	public function set_elem_type($elem_type){
		$this->_elem_type = $elem_type;
		return $this;
	}

	/**
	 * @return int|NULL
	 */
	public function get_elem_type(){
		return $this->_elem_type;
	}

	/**
	 * @param int $elem_id
	 *
	 * @return $this
	 */
	public function set_elem_id($elem_id){
		$this->_elem_id = $elem_id;
		return $this;
	}

	/**
	 * @return int|NULL
	 */
	public function get_elem_id(){
		return $this->_elem_id;
	}


	/**
	 * @param string $val
	 *
	 * @return $this
	 */
	public function set_val($val){
		$this->_val = $val;
		return $this;
	}

	/**
	 * @param int $date
	 *
	 * @return $this
	 */
	public function set_date($date){
		$this->_date = $date;
		return $this;
	}

	/**
	 * @param int $type
	 *
	 * @return $this
	 */
	public function set_type($type){
		$this->_type = $type;
		return $this;
	}


	/**
	 * @return string|NULL
	 */
	public function get_val(){
		return $this->_val;
	}

	/**
	 * @return int|NULL
	 */
	public function get_date(){
		return $this->_date;
	}

	/**
	 * @return int|NULL
	 */
	public function get_type(){
		return $this->_type;
	}

	/**
	 * @return int|NULL
	 */
	public function get_complited(){
		return $this->_complited;
	}


	/**
	 *
	 * @return $this
	 */
	public function create(){
		$this->_CRUD->create_tasks([$this]);
		return $this;
	}

	/**
	 * @return Task
	 */
	public function close(){
		$this->_complited = self::COMPLITED;
		$this->_CRUD->update_tasks([$this]);
		return $this;
	}


}
