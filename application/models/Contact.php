<?php

namespace application\models;

use application\core\CRUD;

class Contact extends Main_Elem {
	const ELEM_TYPE = 1;

	/**
	 * @param int $first_row
	 *
	 * @return array Contacts
	 */
	public function get_list($first_row = 0){
		return $this->_CRUD->list_contacts($first_row);
	}

	/**
	 *
	 * @return $this
	 */
	public function update(){
		$this->_CRUD->update_contacts([$this]);
		return $this;
	}

	/**
	 * @return $this
	 */
	public function create(){
		$this->_CRUD->create_contacts([$this]);
		return $this;
	}
}

