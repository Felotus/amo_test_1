<?php

namespace application\models;

use application\core\CRUD;

abstract class Base_Elem {

	protected $_id;
	protected $_CRUD;
	protected $_create_date;

	/**
	 * Main_Elem constructor.
	 *
	 * @param CRUD $CRUD
	 */
	public function __construct(CRUD $CRUD){
		$this->_CRUD = $CRUD;
	}

	/**
	 * @param int $id|NULL
	 *
	 * @return $this
	 *
	 */
	public function set_id($id){
		$this->_id = $id;
		return $this;
	}

	/**
	 * @return int|NULL
	 */
	public function get_id(){
		return $this->_id;
	}

	/**
	 * @param int $create_date|NULL
	 *
	 * @return $this
	 *
	 */
	public function set_create_date($create_date){
		$this->_create_date = $create_date;
		return $this;
	}

	/**
	 * @return int|NULL
	 */
	public function get_create_date(){
		return $this->_create_date;
	}
}
