<?php

namespace application\models;

use application\core\CRUD;

class Note extends Base_Elem {
	private $_type;
	private $_val;
	private $_call_link;
	private $_call_duration;
	private $_elem_id;
	private $_elem_type;
	const TYPE_NOTE = 4;
	const TYPE_IN_CALL = 10;


	/**
	 * @param int $elem_type
	 *
	 * @return $this
	 */
	public function set_elem_type($elem_type){
		$this->_elem_type = $elem_type;
		return $this;
	}

	/**
	 * @return int|NULL
	 */
	public function get_elem_type(){
		return $this->_elem_type;
	}

	/**
	 * @param int $elem_id
	 *
	 * @return $this
	 */
	public function set_elem_id($elem_id){
		$this->_elem_id = $elem_id;
		return $this;
	}

	/**
	 * @return int|NULL
	 */
	public function get_elem_id(){
		return $this->_elem_id;
	}


	/**
	 * @param string $value
	 *
	 * @return $this
	 */
	public function set_call_link($value){
		$this->_call_link = $value;
		return $this;
	}

	/**
	 * @param string $value
	 *
	 * @return $this
	 */
	public function set_call_duration($value){
		$this->_call_duration = $value;
		return $this;
	}

	/**
	 * @return string|NULL
	 */
	public function get_call_link(){
		return $this->_call_link;
	}

	/**
	 * @return int|NULL
	 */
	public function get_call_duration(){
		return $this->_call_duration;
	}


	/**
	 * @param string $val
	 *
	 * @return $this
	 */
	public function set_val($val){
		$this->_val = $val;
		return $this;
	}

	/**
	 * @param int $type
	 *
	 * @return $this
	 */
	public function set_type($type){
		$this->_type = $type;
		return $this;
	}


	/**
	 * @return string|NULL
	 */
	public function get_val(){
		return $this->_val;
	}

	/**
	 * @return int|NULL
	 */
	public function get_type(){
		return $this->_type;
	}

	/**
	 * @return Note
	 */
	public function create(){
		$this->_CRUD->create_notes([$this]);
		return $this;
	}
}

