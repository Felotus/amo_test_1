<?php

namespace application\models;

use application\core\CRUD;
use application\models\Base_Elem;

abstract class Main_Elem extends Base_Elem {
	protected $_name;
	protected $_custom_fields;




	/**
	 * @return array Field|NULL
	 */
	public function get_custom_fields(){
		return $this->_custom_fields;
	}

	/**
	 * @param array Field $custom_fields
	 *
	 * @return $this
	 */
	public function set_custom_fields(array $custom_fields){
		$this->_custom_fields = $custom_fields;
		return $this;
	}



	/**
	 * @param string $name
	 *
	 * @return $this
	 */
	public function set_name($name){
		$this->_name = $name;
		return $this;
	}



	/**
	 * @return string|NULL
	 */
	public function get_name(){
		return $this->_name;
	}

	/**
	 * @return int|NULL
	 */
	public function get_type(){
		return static::ELEM_TYPE;
	}

}

