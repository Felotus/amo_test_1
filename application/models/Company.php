<?php

namespace application\models;

class Company extends Main_Elem {
	private $_contacts;
	const ELEM_TYPE = 3;

	/**
	 * @param array Contact $contacts
	 *
	 * @return $this
	 */
	public function set_contacts(array $contacts){
		$this->_contacts = $contacts;
		return $this;
	}

	/**
	 * @return array Contact
	 */
	public function get_contacts(){
		return $this->_contacts;
	}

	/**
	 * @param int $first_row
	 *
	 * @return array Company
	 */
	public function get_list($first_row = 0){
		return $this->_CRUD->list_companies($first_row);
	}

	/**
	 *
	 * @return $this
	 */
	public function update(){
		$this->_CRUD->update_contacts([$this]);
		return $this;
	}

	/**
	 * @return $this
	 */
	public function create(){
		$this->_CRUD->create_contacts([$this]);
		return $this;
	}
}

