<?php

namespace application\lib;

use application\lib\Data_Helper;
use application\lib\Curl_Req;
use application\core\CRUD;
use application\models\Company;
use application\models\Contact;
use application\models\Customer;
use application\models\Lead;
use application\models\Field;
use application\models\Task;
use application\models\Note;
use application\models\Main_Elem;
use application\lib\Eternity_Factory;
use Exception;

class Amo_CRUD implements CRUD {
	private $_link;

	private $_params;
	private $_mail;
	private $_hash;
	private $_cookie_time;
	private $_max_row;
	private $_factory;
	private $_origin;
	const ELEM_LINK = [
		1 => 'contacts',
		2 => 'leads',
		3 => 'companies',
		12 => 'customers',
	];
	const COOKIE = 'application/lib/cookie.txt';


	/**
	 * Amo_CRUD constructor.
	 */
	public function __construct(){
		$config = Data_Helper::get_config('AMO_config');
		$this->_link = "https://" . $config['acc'] . ".amocrm.ru/";
		$this->_params[] = $config['api'];
		$this->_mail = $config['mail'];
		$this->_hash = $config['hash'];
		$this->_max_row = $config['max_row'];
		$this->_origin = $this->_hash . '_' . $config['origin'];
		$this->_factory = new Eternity_Factory($this);
		if ($this->auth()) {
			if (file_exists(self::COOKIE)) {
				$this->set_cookie_time();
			} else {
				throw new Exception('cookie файл не найден');
			}
		} else {
			throw new Exception('авторизация не пройдена');
		}
	}




	/**
	 *
	 */
	private function set_cookie_time(){
		$cookie_data = Data_Helper::parse_cookie(self::COOKIE);
		if ($cookie_data[17] === urlencode($this->_mail)) {
			$this->_cookie_time = intval($cookie_data[29]);
		}

	}

	/**
	 * @return bool
	 * @throws Exception
	 */
	private function auth(){
		$data = [
			'USER_LOGIN' => $this->_mail,
			'USER_HASH' => $this->_hash,
		];
		$link = $this->_link . "private/api/auth.php?type=json";
		$result = Curl_Req::query($link, $this->_params, $data);
		$result = $result['response'];
		if (!isset($result['auth'])) {
			$result = FALSE;
		} else {
			$result = TRUE;
		}
		return $result;

	}

	/**
	 * @param array Field $fields
	 *
	 * @return array Field
	 * @throws Exception
	 */
	public function create_fields(array $fields){
		$all_array = array_chunk($fields, $this->_max_row);
		foreach ($all_array as $key => $v) {
			$data = [];
			foreach ($v as $value) {
				$data['add'][] = [
					'name' => $value->get_name(),
					'field_type' => $value->get_type(),
					'element_type' => $value->get_elem_type(),
					'origin' => $this->_origin,
					'enums' => $value->get_enums(),
					'is_editable' => $value->get_editable(),
				];
			}
			$result = Curl_Req::query($this->_link . 'api/v2/fields', $this->_params, $data);
			if (is_array($result)) {
				$result = $result['_embedded']['items'];
				foreach ($result as $k => $val) {
					$fields[($k + ($key * $this->_max_row))]->set_id($val['id']);
				}
			} else {
				throw new Exception('Сервер прислал неожиданный ответ');
			}
		}
		return $fields;
	}

	/**
	 * @param int $first_row
	 *
	 * @return array Main_Elem|NULL
	 * @throws Exception
	 */
	public function list_contacts($first_row = 0){
		$elems = [];
		$start_row = $first_row;
		do {
			$link = $this->_link . 'api/v2/contacts?limit_rows=' . $this->_max_row . '&limit_offset=' . $start_row;
			$result = Curl_Req::query($link, $this->_params);

			if (is_array($result)) {
				$result = $result['_embedded']['items'];
				foreach ($result as $key => $value) {
					$elem = $this->_factory->create_elem('Contact');
					$elem->set_id($value['id']);
					$elems[] = $elem;
				}
			}

			$start_row += $this->_max_row;
		} while (is_array($result));
		return $elems;
	}

	/**
	 * @param int $first_row
	 *
	 * @return array Main_Elem|NULL
	 * @throws Exception
	 */
	public function list_leads($first_row = 0){
		$elems = [];
		$start_row = $first_row;
		do {
			$link = $this->_link . 'api/v2/leads?limit_rows=' . $this->_max_row . '&limit_offset=' . $start_row;
			$result = Curl_Req::query($link, $this->_params);

			if (is_array($result)) {
				$result = $result['_embedded']['items'];
				foreach ($result as $key => $value) {
					$elem = $this->_factory->create_elem('Lead');
					$elem->set_id($value['id']);
					$elems[] = $elem;
				}
			}
			$start_row += $this->_max_row;
		} while (is_array($result));
		return $elems;
	}

	/**
	 * @param int $first_row
	 *
	 * @return array Main_Elem|NULL
	 * @throws Exception
	 */
	public function list_customers($first_row = 0){
		$elems = [];
		$start_row = $first_row;
		do {
			$link = $this->_link . 'api/v2/customers?limit_rows=' . $this->_max_row . '&limit_offset=' . $start_row;
			$result = Curl_Req::query($link, $this->_params);

			if (is_array($result)) {
				$result = $result['_embedded']['items'];
				foreach ($result as $key => $value) {
					$elem = $this->_factory->create_elem('Customer');
					$elem->set_id($value['id']);
					$elems[] = $elem;
				}
			}
			$start_row = +$this->_max_row;
		} while (is_array($result));
		return $elems;
	}

	/**
	 * @param int $first_row
	 *
	 * @return array Main_Elem|NULL
	 * @throws Exception
	 */
	public function list_companies($first_row = 0){
		$elems = [];
		$start_row = $first_row;
		do {
			$link = $this->_link . 'api/v2/companies?limit_rows=' . $this->_max_row . '&limit_offset=' . $start_row;
			$result = Curl_Req::query($link, $this->_params);

			if (is_array($result)) {
				$result = $result['_embedded']['items'];
				foreach ($result as $key => $value) {
					$elem = $this->_factory->create_elem('Company');
					$elem->set_id($value['id']);
					$elems[] = $elem;
				}
			}
			$start_row = +$this->_max_row;
		} while (is_array($result));
		return $elems;
	}

	/**
	 * @param array Customer $customers
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function update_customers(array $customers){
		return $this->update_elems($customers);
	}

	/**
	 * @param array lead $leads
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function update_leads(array $leads){
		return $this->update_elems($leads);
	}

	/**
	 * @param array Contact $contacts
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function update_contacts(array $contacts){
		return $this->update_elems($contacts);
	}


	/**
	 * @param array Company $companies
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function update_companies(array $companies){
		return $this->update_elems($companies);
	}

	/**
	 * @param array $elems
	 *
	 * @return bool
	 * @throws Exception
	 */
	private function update_elems(array $elems){
		if (count($elems) > 0) {
			$elem_type = $elems[0]->get_type();
			$all_array = array_chunk($elems, $this->_max_row);
			foreach ($all_array as $key => $val) {
				$data = [];
				foreach ($val as $k => $v) {
					$custom_fields = [];
					foreach ($v->get_custom_fields() as $val) {
						$values = $val->get_values();
						switch ($val->get_type()) {
							case Field::TEXT:
								$values[0] = ['value' => $values[0]];
								break;

							case Field::MULTISELECT:
								break;

							default:
								throw new Exception('Неизвестный тип поля');
								break;
						}
						$custom_fields[] = [
							'id' => $val->get_id(),
							'values' => $values,
						];
					}
					$data['update'][] = [
						'id' => $v->get_id(),
						'updated_at' => time(),
						'custom_fields' => $custom_fields,
					];
				}
				Curl_Req::query($this->_link . 'api/v2/' . self::ELEM_LINK[$elem_type], $this->_params, $data);
			}
		}
		return TRUE;
	}


	/**
	 * @param int $elem_type
	 *
	 * @return array Field
	 * @throws Exception
	 */
	public function list_fields($elem_type){
		$id = NULL;
		$enums = [];
		$fields = [];
		$result = $this->acc_req(['custom_fields']);
		if (is_array($result)) {
			$result = $result['_embedded']['custom_fields'][self::ELEM_LINK[$elem_type]];
			foreach ($result as $key => $value) {
				if (isset($value['enums'])) {
					$enums = $value['enums'];
				}
				$field = $this->_factory->create_elem('Field');;
				$field->set_name($value['name'])
					->set_enums($enums)
					->set_id($key)
					->set_type($value['field_type'])
					->set_elem_type($elem_type);
				$fields[] = $field;
			}
		} else {
			throw new Exception('Сервер прислал неожиданный ответ');
		}
		return $fields;
	}


	/**
	 * @param array|NULL $params
	 *
	 * @return array
	 * @throws Exception
	 */
	private function acc_req(array $params = NULL){
		$link = $this->_link . 'api/v2/account';
		if (!is_null($params)) {
			$link .= '?with=' . implode(',', $params);
		}
		implode(',', $params);
		return Curl_Req::query($link, $this->_params);
	}


	/**
	 * @param array Note $note
	 *
	 * @throws Exception
	 */
	public function create_notes(array $note){
		$all_array = array_chunk($note, $this->_max_row);
		foreach ($all_array as $val) {
			$data = [];
			foreach ($val as $v) {
				$data['add'][] = [
					'element_id' => $v->get_elem_id(),
					'element_type' => $v->get_elem_type(),
					'note_type' => $v->get_type(),
				];
				switch ($v->get_type()) {
					case Note::TYPE_NOTE:
						$data['add'][0]['text'] = $v->get_val();
						break;
					case Note::TYPE_IN_CALL:
						$data['add'][0]['params']['UNIQ'] = $this->_origin;
						$data['add'][0]['params']['DURATION'] = $v->get_call_duration();
						$data['add'][0]['params']['SRC'] = $v->get_call_link();
						$data['add'][0]['params']['LINK'] = $v->get_call_link();
						$data['add'][0]['params']['PHONE'] = $v->get_val();
						break;
					default:
						$data['add'][0]['text'] = $v->get_val();
						break;
				}
			}

			$result = Curl_Req::query($this->_link . 'api/v2/notes', $this->_params, $data);
			if (isset($result['_embedded']['items'][0]['id']) && $result['_embedded']['items'][0]['id'] === 0) {
				throw new Exception('не удалось найти элемент');
			} elseif (!isset($result['_embedded']['items'])) {
				throw new Exception('Сервер прислал неожиданный ответ');
			}
		}
	}

	/**
	 * @return array
	 * @throws Exception
	 */
	public function get_task_types(){
		$tasks = [];
		$result = $this->acc_req(['task_types']);
		$result = $result['_embedded']['task_types'];
		foreach ($result as $key => $value) {
			$tasks[$value['id']] = $value['name'];
		}
		return $tasks;
	}


	/**
	 * @param array Task $task
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function create_tasks(array $task){
		$all_array = array_chunk($task, $this->_max_row);
		foreach ($all_array as $val) {
			$data = [];
			foreach ($val as $v) {
				$data['add'][] = [
					'element_id' => $v->get_elem_id(),
					'element_type' => $v->get_elem_type(),
					'task_type' => $v->get_type(),
					'complete_till' => $v->get_date(),
					'text' => $v->get_val(),
				];
			}
			$result = Curl_Req::query($this->_link . 'api/v2/tasks', $this->_params, $data);
			if (isset($result['_embedded']['items'][0]['id']) && $result['_embedded']['items'][0]['id'] === 0) {
				throw new Exception('не удалось найти элемент');
			} elseif (!isset($result['_embedded']['items'])) {
				throw new Exception('Сервер прислал неожиданный ответ');
			}
		}
		return TRUE;
	}


	/**
	 * @param array Task $task
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function update_tasks(array $task){
		$all_array = array_chunk($task, $this->_max_row);
		foreach ($all_array as $val) {
			$data = [];
			foreach ($val as $v) {
				$data['update'][] = [
					'id' => $v->get_id(),
					'is_completed' => $v->get_complited(),
					'updated_at' => time(),
					'text' => $v->get_val(),
				];
			}
			$result = Curl_Req::query($this->_link . 'api/v2/tasks', $this->_params, $data);
			if (isset($result['_embedded']['items'][0]['id']) && $result['_embedded']['items'][0]['id'] === 0) {
				throw new Exception('не удалось найти элемент');
			} elseif (!isset($result['_embedded']['items'])) {
				throw new Exception('Сервер прислал неожиданный ответ');
			}
		}
		return TRUE;
	}

	/**
	 * @param array Lead $contacts
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function create_leads(array $leads){
		return $this->create_elems($leads);
	}

	/**
	 * @param array Customer $contacts
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function create_customers(array $customers){
		return $this->create_elems($customers);
	}

	/**
	 * @param array Company $contacts
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function create_companies(array $companies){
		return $this->create_elems($companies);
	}

	/**
	 * @param array Contact $contacts
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function create_contacts(array $contacts){
		return $this->create_elems($contacts);
	}

	/**
	 * @param array Main_Elem  $elems
	 *
	 * @return bool
	 * @throws Exception
	 */
	private function create_elems(array $elems){
		$elem_type = $elems[0]->get_type();
		$all_array = array_chunk($elems, $this->_max_row);
		foreach ($all_array as $key => $val) {
			$data = [];
			foreach ($val as $k => $v) {
				$custom_fields = [];
				$data['add'][$k]['name'] = $v->get_name();
				switch (TRUE) {
					case ($elem_type === Lead::ELEM_TYPE || $elem_type === Customer::ELEM_TYPE):
						$data['add'][$k]['company_id'] = $v->get_company();
					case ($elem_type !== Contact::ELEM_TYPE):
						$data['add'][$k]['contacts_id'] = $v->get_contacts();
						break;
					case ($elem_type === Contact::ELEM_TYPE):
						foreach ($v->get_custom_fields() as $val) {
							$values = $val->get_values();
							switch ($val->get_type()) {
								case Field::TEXT:
									$values[0] = ['value' => $values[0]];
									break;

								case Field::MULTISELECT:
									break;

								default:
									throw new Exception('Неизвестный тип поля');
									break;
							}
							$custom_fields[] = [
								'id' => $val->get_id(),
								'values' => $values,
							];
							$data['add'][$k]['custom_fields'] = $custom_fields;
						}
						break;
					default:
						break;
				}
			}
			$result = Curl_Req::query($this->_link . 'api/v2/' . self::ELEM_LINK[$elem_type], $this->_params, $data);
			if (is_array($result)) {
				$result = $result['_embedded']['items'];
				foreach ($result as $k => $v) {
					$elems[$k + ($key * $this->_max_row)]->set_id($v['id']);
				}
			} else {
				throw new Exception('Сервер прислал неожиданный ответ');
			}
		}
		return TRUE;
	}

	/**
	 * @param array Field $fields
	 *
	 * @return array
	 * @throws Exception
	 */
	public function delete_fields(array $fields){
		$data = [];
		foreach ($fields as $v) {
			$data['delete'][] = [
				'id' => $v->get_id(),
				'origin' => $this->_origin,
			];
		}
		return Curl_Req::query($this->_link . 'api/v2/fields', $this->_params, $data);

	}

	public function list_leads_ids(array $ids) {
		$leads = [];
		foreach (array_chunk($ids, $this->_max_row) as $chunk_ids) {
			$str_ids = '';
			foreach ($chunk_ids as $id) {
				$str_ids .= 'id[]=' . $id . '&';
			}
			$result = Curl_Req::query($this->_link . 'api/v2/leads?' . $str_ids, $this->_params);
			foreach ($result['_embedded']['items'] as $item) {
				$contacts = [];
				$tags = [];
				$custom_fields = [];
				foreach ($item['contacts']['id'] as $contact_id) {
					$contacts[] = $contact_id;
				}
				foreach ($item['tags'] as $tag) {
					$tags[$tag['id']] = $tag['name'];
				}
				foreach ($item['custom_fields'] as $custom_field_item) {
					$custom_field = $this->_factory->create_elem('field');
					$values = [];
					foreach ($custom_field_item['values'] as $key => $value) {
						if (array_key_exists('enum', $value)) {
							$key = $value['enum'];
						}
						$values[$key] = $value['value'];

					}
					$custom_field->set_id($custom_field_item['id'])
						->set_name($custom_field_item['name'])
						->set_values($values);
					$custom_fields[] = $custom_field;
				}
				$lead = $this->_factory->create_elem('lead');
				$lead->set_id($item['id'])
					->set_name($item['name'])
					->set_tags($tags)
					->set_create_date($item['created_at'])
					->set_custom_fields($custom_fields)
					->set_company($item['company']['id'])
					->set_contacts($contacts);
				$leads[] = $lead;
			}
		}
		return $leads;
	}

	public function list_contacts_ids(array $ids) {
		$contacts =[];
		foreach (array_chunk($ids, $this->_max_row) as $chunk_ids) {
			$str_ids = '';
			foreach ($chunk_ids as $id) {
				$str_ids .= 'id[]=' . $id . '&';
			}

			$result = Curl_Req::query($this->_link . 'api/v2/contacts?' . $str_ids, $this->_params);
			foreach ($result['_embedded']['items'] as $item) {
				$contact = $this->_factory->create_elem('contact');
				$contact->set_id($item['id'])
					->set_name($item['name']);
				$contacts[] = $contact;
			}
		}
		return $contacts;
	}
	public function list_companies_ids(array $ids) {
		$companies =[];
		foreach (array_chunk($ids, $this->_max_row) as $chunk_ids) {
			$str_ids = '';
			foreach ($chunk_ids as $id) {
				$str_ids .= 'id[]=' . $id . '&';
			}

			$result = Curl_Req::query($this->_link . 'api/v2/companies?' . $str_ids, $this->_params);
			foreach ($result['_embedded']['items'] as $item) {
				$company = $this->_factory->create_elem('company');
				$company->set_id($item['id'])
					->set_name($item['name']);
				$companies[] = $company;
			}
		}
		return $companies;
	}
}

