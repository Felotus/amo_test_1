<?php

namespace  application\lib;

use application\core\Data_Storage;
use Exception;

abstract class Curl_Req implements Data_Storage {

	const FLAG = 0;


	/**
	 * @param string $link
	 * @param array|NULL $data
	 * @param array $params
	 *
	 * @return array
	 * @throws Exception
	 */
	static public function query($link, array $params, array $data = NULL){
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($curl, CURLOPT_USERAGENT, $params[0]);
		curl_setopt($curl, CURLOPT_URL, $link);
		curl_setopt($curl, CURLOPT_HEADER, FALSE);
		curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . "/cookie.txt");
		curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . "/cookie.txt");
		if (!is_null($data)) {
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		}
		$out = curl_exec($curl);
		$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
		$code = (int)$code;
		$errors = [
			301 => 'Moved permanently',
			400 => 'Bad request',
			401 => 'Unauthorized',
			403 => 'Forbidden',
			404 => 'Not found',
			500 => 'Internal server error',
			502 => 'Bad gateway',
			503 => 'Service unavailable',
			504 => 'Serer problem',
		];
		usleep(142857);

		$flag = self::FLAG;
		if (($code === 429 || $code === 504) && $flag < self::FLAG_LIMIT) {
			sleep(30);
			$flag++;
			$result = self::query($link, $data, $params);
		} elseif ($code !== 200 && $code !== 204) {
			if (isset($errors[$code])) {
				throw new Exception($errors[$code], $code);
			} else {
				throw new Exception('Undescripted error', $code);
			}
		} else {
			$result = json_decode($out, TRUE);
		}
		return $result;
	}
}

