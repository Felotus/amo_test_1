<?php
namespace application\lib;

abstract class Data_Helper {
	/**
	 * @param string $value
	 *
	 * @return string
	 */
	static function clear($value = ''){
		$value = trim($value);
		$value = htmlspecialchars($value);
		return $value;
	}


	/**
	 * @param string $link
	 *
	 * @return array
	 */
	static function parse_cookie($link){
		$lines = file($link);
		$tokens = [];
		$result =[];
		foreach($lines as $line) {

			$tokens = explode("\t", $line);
			$tokens = array_map('trim', $tokens);
			$result = array_merge($result, $tokens);
		}
		return $result;
	}




	/**
	 * @param $config
	 *
	 * @return array
	 */
	static function get_config($config){
		$path = 'application/config/'.$config.'.php';
		if (file_exists($path)){
			$result = require $path;
		} else {
			$result = [];
		}
		return $result;
	}
}
