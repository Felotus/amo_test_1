<?php

namespace application\lib;

use application\core\CRUD;
use application\models\Base_Elem;

class Eternity_Factory {

	private $_CRUD;
	const ELEM_NAME = [
		1 => 'contact',
		2 => 'lead',
		3 => 'company',
		12 => 'customer',
	];


	public function __construct(CRUD $CRUD){
		$this->_CRUD = $CRUD;
	}

	/**
	 * @param string $model_name
	 *
	 * @return Base_Elem|NULL
	 */
	public function create_elem($model_name){
		$model_name = 'application\models\\' . ucfirst($model_name);
		if (class_exists($model_name)) {
			$result = new $model_name($this->_CRUD);
		} else {
			$result = NULL;
		}
		return $result;
	}

	/**
	 * @param string $model_name
	 * @param int $num
	 *
	 * @return array Base_Elem
	 */
	public function create_mass_elem($model_name, $num){
		$model_name = 'application\models\\' . ucfirst($model_name);
		if (class_exists($model_name)) {
			for ($i = 0; $i < $num; $i++) {
				$result[] = new $model_name($this->_CRUD);
			}
		} else {
			$result = [];
		}
		return $result;
	}


	/**
	 * @param int $elem_type
	 *
	 * @return NULL|Base_Elem
	 */
	public function create_elem_by_type($elem_type){
		if (!is_null(self::ELEM_NAME[$elem_type])) {
			$result = $this->create_elem(self::ELEM_NAME[$elem_type]);
		} else {
			$result = NULL;
		}
		return $result;
	}


	/**
	 * @param int $elem_type
	 * @param int $num
	 *
	 * @return array Base_Elem
	 */
	public function create_mass_elem_by_type($elem_type, $num){
		if (!is_null(self::ELEM_NAME[$elem_type])) {
			$result = $this->create_mass_elem(self::ELEM_NAME[$elem_type], $num);
		} else {
			$result = [];
		}
		return $result;
	}

}
